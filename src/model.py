from dataclasses import dataclass


# It is good practice to model the contract in dataclasses in order to have a defined interface for tests
@dataclass
class Request:
    request: str


@dataclass
class Response:
    response: str
