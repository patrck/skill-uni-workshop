import json
from urllib.request import urlopen

from src.model import Request, Response


class EchoService:

    def __init__(self, transform_mode: str = "lowercase"):
        self._transform_mode: str = transform_mode

    def echo(self, request: Request) -> Response:
        data: dict = json.loads(urlopen(f"https://postman-echo.com/get?text={request.request}").read())

        data_as_string: str = json.dumps(data["args"])
        return Response(self._transform(data_as_string))

    def _transform(self, data: str):

        if self._transform_mode == "lowercase":
            return data.lower()
        elif self._transform_mode == "uppercase":
            return data.upper()
        else:
            raise ValueError(f"Mode {self._transform_mode} is not supported")
